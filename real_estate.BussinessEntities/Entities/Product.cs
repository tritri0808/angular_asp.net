﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace real_estate.BussinessEntities.Entities
{
    [Table("product")]
    public partial class Product: Entity<int>
    {
        [StringLength(50)]
        public string ProductName { get; set; }

        public double? Price { get ; set; }

        [Column(TypeName = "text")]
        public string Image { get; set; }

    }
}
