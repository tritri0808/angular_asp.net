USE [real_estate]
GO
/****** Object:  Table [dbo].[product]    Script Date: 4/4/2017 4:03:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](50) NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[product] ON 

INSERT [dbo].[product] ([Id], [ProductName]) VALUES (1, N'Product 1234')
INSERT [dbo].[product] ([Id], [ProductName]) VALUES (2, N'Product 2')
INSERT [dbo].[product] ([Id], [ProductName]) VALUES (3, N'Product 3')
SET IDENTITY_INSERT [dbo].[product] OFF
