﻿using System.Web;
using System.Web.Optimization;

namespace real_estate
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                     "~/Scripts/angular.js",
                     "~/Scripts/angular-cookies.js",
                     "~/Scripts/angular-sanitize.js",
                     "~/Scripts/angular-animate.js"));

            bundles.Add(new ScriptBundle("~/bundles/spa")
                     .Include("~/Scripts/spa/app.js")
                     .IncludeDirectory("~/Scripts/spa/Services", "*.js")
                     .IncludeDirectory("~/Scripts/spa/Controller", "*.js"));
                     //"~/Scripts/spa/Services/eventFactory.js",
                     //"~/Scripts/spa/Services/productFactory.js",
                     //"~/Scripts/spa/Controller/productController.js",
                     //"~/Scripts/spa/Controller/storeController.js",
                     //"~/Scripts/spa/Controller/eventController.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css"));
        }
    }
}
