﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(real_estate.WebApp.Startup))]
namespace real_estate.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
    
        }
    }
}