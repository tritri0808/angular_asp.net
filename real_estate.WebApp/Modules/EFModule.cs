﻿using Autofac;
using real_estate.BussinessEntities;
using real_estate.DAL.Infrastructure;
using System.Data.Entity;


namespace real_estate.WebApp.Modules
{
    public class EFModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule());

            builder.RegisterType(typeof(RealEsateContext)).As(typeof(DbContext)).InstancePerLifetimeScope();

            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerRequest();

        }

    }
}