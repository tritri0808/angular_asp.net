﻿using real_estate.BussinessEntities;
using real_estate.BussinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace real_estate.WebApp.Controllers
{
    [AllowAnonymous]

    public class ProductApiController : ApiController
    {
        //Api/ProductApi/Product
        [HttpGet]
        public List<Product> Product()
        {
            RealEsateContext context = new RealEsateContext();            
            return context.Products.ToList();
        }
    }
}
