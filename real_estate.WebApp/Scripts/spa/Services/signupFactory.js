﻿(function () {
    'use strict';

    var serviceId = 'signupFactory';

    angular.module('RealEstateApp').factory(serviceId,
        ['$http', signupFactory]);

    function signupFactory($http) {

        function getUserExists() {
            return $http.get('/Account/IsUserAvailable');
        }

        var service = {
            getUserExists: getUserExists
        };

        return service;
    }
})();