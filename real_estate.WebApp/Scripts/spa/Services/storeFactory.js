﻿(function () {
    'use strict';

    var serviceId = 'storeFactory';

    angular.module('RealEstateApp').factory(serviceId,
        ['$http', storeFactory]);

    function storeFactory($http) {

        function getProducts() {
            return $http.get('Api/ProductApi/Product');
        }

        var service = {
            getProducts: getProducts
        };

        return service;
    }

})();