﻿(function () {
    'use strict';

    var serviceId = 'productFactory';

    angular.module('RealEstateApp').factory(serviceId,
        ['$http', productFactory]);

    function productFactory($http) {

        function getProduct() {
            return $http.get('/api/products');
        }

        var service = {
            getProduct: getProduct
        };

        return service;
    }
})();