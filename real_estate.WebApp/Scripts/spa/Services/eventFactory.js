﻿(function () {
    'use strict';

    var serviceId = 'eventFactory';

    angular.module('RealEstateApp').factory(serviceId,
        ['$http', eventFactory]);

    function eventFactory($http) {

        function getTalks() {
            return $http.get('/Home/GetTalkDetails');
        }
    
        var service = {
            getTalks: getTalks
        };

        return service;
    }

})();