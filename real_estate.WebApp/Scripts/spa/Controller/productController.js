﻿(function () {
    'use strict';

    var controllerId = 'productController';

    angular.module('RealEstateApp').controller(controllerId,
        ['$scope', 'productFactory', productController]);
    
    function productController($scope, productFactory) {
        $scope.products = [];

        productFactory.getProduct().then(
            // callback function for successful http request
            function success(response) {
                $scope.products = response.data;
            },
            // callback function for error in http request
            function error(response) {
                // log errors
            }
        );
    }
})();