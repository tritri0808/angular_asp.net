﻿(function () {
    'use strict';

    var controllerId = 'signupController';

    angular.module('RealEstateApp').controller(controllerId,
        ['$scope', 'signupFactory', signupController]);

    function signupController($scope, signupFactory) {
        $scope.person = [];

        signupFactory.getUserExists().then(
            // callback function for successful http request
            function success(response) {
                $scope.person = response.data;
            },
            // callback function for error in http request
            function error(response) {
                // log errors
            }
        );
    }
})();