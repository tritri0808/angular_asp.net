﻿(function () {
    'use strict';
    var controllerId = 'eventController';

    angular.module('RealEstateApp').controller(controllerId,
        ['$scope', 'eventFactory', eventController]);
    function eventController($scope, eventFactory) {
        $scope.talks = [];

        eventFactory.getTalks().then(
            // callback function for successful http request
            function success(response) {
                $scope.talks = response.data;
                console.log($scope.talks);

            },
            // callback function for error in http request
            function error(response) {
                // log errors
            }
        );
    }
})();
