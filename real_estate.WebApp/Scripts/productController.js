﻿function productController($scope, $http) {
    $scope.loading = true;
    $scope.addMode = false;

    //Used to display the data 
    $http.get('/api/Products/').success(function (data) {
        $scope.Products = data;
        $scope.loading = false;
    })
    .error(function () {
        $scope.error = "An Error has occured while loading posts!";
        $scope.loading = false;
    });

    $scope.toggleEdit = function () {
        this.Product.editMode = !this.Product.editMode;
    };
    $scope.toggleAdd = function () {
        $scope.addMode = !$scope.addMode;
    };

    //Used to save a record after edit 
    $scope.save = function () {
        alert("Edit");
        $scope.loading = true;
        var frien = this.Product;
        alert(emp);
        $http.put('/api/Products/', frien).success(function (data) {
            alert("Saved Successfully!!");
            emp.editMode = false;
            $scope.loading = false;
        }).error(function (data) {
            $scope.error = "An Error has occured while Saving Products! " + data;
            $scope.loading = false;

        });
    };

    //Used to add a new record 
    $scope.add = function () {
        $scope.loading = true;
        $http.post('/api/Products/', this.newProduct).success(function (data) {
            alert("Added Successfully!!");
            $scope.addMode = false;
            $scope.Products.push(data);
            $scope.loading = false;
        }).error(function (data) {
            $scope.error = "An Error has occured while Adding Products! " + data;
            $scope.loading = false;

        });
    };

    //Used to edit a record 
    $scope.deleteProduct = function () {
        $scope.loading = true;
        var Productid = this.Product.Productid;
        $http.delete('/api/Products/' + Productid).success(function (data) {
            alert("Deleted Successfully!!");
            $.each($scope.Products, function (i) {
                if ($scope.Products[i].Productid === Productid) {
                    $scope.Products.splice(i, 1);
                    return false;
                }
            });
            $scope.loading = false;
        }).error(function (data) {
            $scope.error = "An Error has occured while Saving Products! " + data;
            $scope.loading = false;

        });
    };

}