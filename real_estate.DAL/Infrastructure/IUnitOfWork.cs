﻿using real_estate.BussinessEntities.Entities;
using real_estate.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace real_estate.DAL.Infrastructure
{
    public interface IUnitOfWork: IDisposable
    {
        //IGenericRepository<Product> ProductRepository { get;}
        //IGenericRepository<User> UserRepository { get;}
        //IGenericRepository<Role> RoleRepository { get; }
        //IGenericRepository<UserRole> UserRoleRepository { get; }
        //IGenericRepository<Error> ErrorsRepository { get; }
        int Commit();
    }
}
