﻿using real_estate.BussinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace real_estate.DAL.Repositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Product GetById(int id);
    }
}
