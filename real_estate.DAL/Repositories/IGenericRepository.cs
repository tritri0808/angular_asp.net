﻿using real_estate.BussinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace real_estate.DAL.Repositories
{
    public interface IGenericRepository<T>  where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        //T GetSingle(int id);
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        T Add(T entity);
        T Delete(T entity);
        void Edit(T entity);
        void Save();

    }
}
