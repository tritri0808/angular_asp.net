﻿using real_estate.BussinessEntities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace real_estate.BL
{
    public interface IProductService : IEntityService<Product>
    {
        Product GetById(int Id);
    }
}
